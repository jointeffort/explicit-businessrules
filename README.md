# Explicit Coding of Business Rules
This is a demo project showing ways to code business rules in a more explicit way, as _first-class citizens_, in order 
to make code more readable, encourage reuse and make maintenance less painful.

### Problem
Too many times, I struggle finding the implementation of a business rule described in the functional specifications. I
think one of the main causes is that often, 'rules' as a thing, have no explicit counterpart in code. We represent
objects from real life (an order) as classes and objects in code, and processes (register a new customer) as functions 
(or method). Rules then are 'muffled away' in the implementation of those functions. Consider this example:

```kotlin
    if (orderRepository.computeTotalAmountSpentFor(customer) > 10_000 &&
        Period.between(customer.firstOrderDate ?: LocalDate.now(), LocalDate.now()).years >= 5) {
        // add discount
    }
```
I bet you need to read this twice to get the idea that if the order amount is more than 10,000 and the buyer has been
customer for 5 or more years, a discount will be added. Of course, you can make ik more readable like this:

```kotlin
    val totalAmountSpent = orderRepository.computeTotalAmountSpentFor(customer)
    val customerYears = Period.between(customer.firstOrderDate ?: LocalDate.now(), LocalDate.now()).years
    if (totalAmountSpent > 10_000 && customerYears >= 5) {
        // add discount
    }
```
But still, to reuse this logic, you have to copy it. And this is just a simple case.
### Solution
One way to to make this concept more explicit is to introduce the 'VIP Customer' concept as a first class citizen in 
code. In this case, we could use the [**Specification**](https://en.wikipedia.org/wiki/Specification_pattern) design 
pattern to create a _VipCustomerSpecification_ and refactor the code as follows:
```kotlin
if (vipCustomerSpecification.isSatisfiedBy(customer, order)) {
    // add discount
}
```
As the Wikipedia page shows, some handy boolean logic methods can be added to the base class in order to make 
specifications composable.

And you may go as far as you like: sometimes a specification may be used in a simple check like above, but you may want 
to write adapters for specific technology contexts like SQL:
```kotlin
    val cb = em.criteriaBuilder
    val q = cb.createQuery(Customer::class.java)
    vipCustomerSpecification.apply(cb,q, q.from(Customer::class.java))
```
(...leaving the [adapter](https://en.wikipedia.org/wiki/Adapter_pattern) part as an exercise to the reader, you don't want SQL specific code in domain objects)
