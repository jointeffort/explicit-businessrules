package com.example.demo

import org.springframework.stereotype.Component

@Component
class NoopOrderRepository : OrderRepository {
    override fun computeTotalAmountSpentFor(customer: Customer): Long {
        return 5000
    }
}
