package com.example.demo

import jakarta.persistence.EntityManager
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController

@RestController
class DemoController(val em: EntityManager, val orderRepository: OrderRepository) {

    private val vipCustomerSpecification = VipCustomerSpecification(orderRepository)
    @GetMapping("/customers")
    fun getUsers(): List<Customer> {

        val cb = em.criteriaBuilder
        val q = cb.createQuery(Customer::class.java)

        // Criteria variant, applying VIP customer logic to query
        vipCustomerSpecification.apply(cb, q, q.from(Customer::class.java))

        val customers =  em.createQuery(q).resultList

        // POJO variant
        customers.forEach {
            assert(vipCustomerSpecification.isSatisfiedBy(it))
        }

        return customers
    }
}
