package com.example.demo

import jakarta.persistence.criteria.CriteriaBuilder
import jakarta.persistence.criteria.CriteriaQuery
import jakarta.persistence.criteria.Root
import java.time.LocalDate
import java.time.Period

data class Customer(val id: Long, val email: String, val firstOrderDate: LocalDate?)

interface OrderRepository {
    fun computeTotalAmountSpentFor(customer: Customer): Long
}

class VipCustomerSpecification(val orderRepository: OrderRepository) {

    fun isSatisfiedBy(customer: Customer) :  Boolean {
        val totalAmountSpent = orderRepository.computeTotalAmountSpentFor(customer)
        val customerYears = Period.between(customer.firstOrderDate ?: LocalDate.now(), LocalDate.now()).years
        return totalAmountSpent > 10_000 && customerYears >= 5
    }

    fun apply(cb: CriteriaBuilder, q: CriteriaQuery<out Any>, from: Root<Customer>) {
        // add criteria to query to select only VIP customers
    }
}
